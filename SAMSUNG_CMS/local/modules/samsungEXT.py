class CMS:
	def __init__(self, ownerComp):
		self.Owner = ownerComp
		self.ControlPanel = ownerComp.op('controlPanel')
		self.ModeTOP = ownerComp.op('mode')
		self.TextMessage = ownerComp.op('textMessage')
		self.DefaultText = ownerComp.op('defaultText')

	def SwitchMode(self, mode, remoteMode=True):
		print('Mode is *****{}*****'.format(mode))
		patchedMode = mode
		if isinstance(mode, list):
			patchedMode = mode[0][:-1]
		if remoteMode:
			self.ControlPanel.op(patchedMode).click()
		self.ModeTOP.par.top = patchedMode
		return


	def ClearMessage(self):
		self.TextMessage.text = ''

	def SetMessage(self, msg=None):
		if msg:
			if isinstance(msg, list):
				msg = ' '.join(msg)

			self.TextMessage.text = msg
		else:
			self.TextMessage.text = ''

	def SetDefaultText(self):
		self.TextMessage.text = self.DefaultText.text
		return