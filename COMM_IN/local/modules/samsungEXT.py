class CMS_COMM_IN:
	def __init__(self, ownerComp):
		self.Owner = ownerComp
		self.CMS = op.SAMSUNG_CMS


	def SendCMD(self, cmd):
		'''

		:param cmd: modes == on, off, animate
		:return:
		'''
		commands = {
			'mode': 'SwitchMode',
			'clearmessage': 'ClearMessage',
			'save': 'SetMessage',
			'setdefaulttext': 'SetDefaultText'
		}

		if cmd.lower() in commands:
			return commands[cmd.lower()]

	def ParseMessage(self, message):
		'''

		:param message: computerName, function, params

		:return:
		'''
		print(message)
		name, *functionAndParams = message.split()
		cmd = self.SendCMD(functionAndParams[0])
		if cmd == 'SetMessage':
			params = ' '.join(functionAndParams[1:])
		else:

			params = functionAndParams[1:]

		print(name)
		print(cmd)
		print(*params)

		if params:
			getattr(self.CMS, cmd)(params)

		else:
			getattr(self.CMS, cmd)()



	# def SwitchMode(self, mode, remoteMode=True):
	# 	if remoteMode:
	# 		self.ControlPanel.op(mode).click()
	# 	self.ModeTOP.par.top = mode
	# 	return
	#
	#
	# def ClearMessage(self):
	# 	self.TextMessage.text = ''
	#
	# def SetMessage(self, msg):
	# 	self.TextMessage.text = msg
	#
	# def SetDefaultText(self):
	# 	self.TextMessage.text = self.DefaultText.text
	# 	return